# 8. Zukunft der Versorgung mit elektrischer Energie in der Schweiz

## BFE-Energieperspektiven
Seit der Erdölkrise in den 1970er Jahren erarbeitet das Bundesamt für Energie (BFE) periodisch sog. Energieperspektiven. Dabei handelt es sich um Informations- und Prognosesammlungen, welche einen Blick in die Energiezukunft ermöglichen sollen. 
- __2011__ wurde die seit 2004 sich in Arbeit befindende Sammlung __Energieperspektiven 2035__ fertiggestellt. 
- __2013__ hat der Bund in Folge der Kernreaktorkatastrophe in Fukushima (2011) die Energieperspektiven 2035 bis ins Jahr 2050 erweitert und angepasst. Daraus entstanden die __Energieperspektiven 2050__ woraus einerseits die neue Energiestrategie 2050 entstand, in welcher Kernkraft keine Rolle mehr in der zukünftigen Schweizer Energieversorgung spielt. Andererseits entstand daraus das am 1. Januar 2018 in Kraft getretene Energiegesetz.  
- __2020__ hat der Bund die aus den Energieperspektiven 2050 mithilfe von aktuellen Rahmendaten und Technologieentwicklungen aktualisierten __Energieperspektiven 2050+__ veröffentlicht.

## Energieperspektiven 2050+
Die Sammlung __Energieperspektiven 2050+__ zeichnet sich u.a. durch folgende Aspekte aus:
- Keine fossilen Energien mehr
- Mehr elektrische Energie
- Tieferer Pro-Kopf-Verbrauch (dies v.a. aufgrund von Effizienzmassnahmen)
- Vorwiegend inländisch produzierte, erneuerbare Energie
- Weitere Punkte, aufbereitet in folgender Grafik:

![Zielbild der klimaneutralen Schweiz 2050](../img/Zielbild_2050+.png)
###### Abbildung 8.1: Zielbild der klimaneutralen Schweiz 2050; Quelle: Energieperspektiven 2050+, Zusammenfassung der wichtigsten Ergebnisse - Prognos AG, TEP Energy GmbH, Infras AG und Ecoplan AG im Auftrag des BFE 

### Szenarien
Mit den Energieperspektiven 2050+ hat der Bund vier Szenarien der zuküftigen Energiesituation ohne Kernkraft erarbeitet:

- __ZERO-Basisvariante:__ Dieses Basisszenario berücksichtigt im Wesentlichen folgende Aspekte:
	- Rasch und umfassend ansteigende Energieeffizienz
	- Starke Elektrifizierung des Energiesystems (Elektrofahrzeuge ersetzen Fahrzeuge mit Verbrennungsmotor, fossile Heizungen werden durch Wärmepumpen ersetzt)
	- Wärmenetze mit erneuerbaren Energien
	- Deutliche Zunahme von energetischer Biomasse-Verwertung
	- Einsatz von strombasierten Energieträgern (Synfuels, Wasserstoff)
	- Massiver Ausbau der Stromproduktion
	- CO2-Abscheidungs- und Einlagerungstechnologien zum Ausgleich von Restemissionen
- __ZERO-Variante A:__ Dieses Ergänzungsszenario (als Ergänzung zur ZERO-Basisvariante) fokussiert den Aspekt der Elektrifizierung des Energiesystems noch stärker als das Basisszenario.
- __ZERO-Variante B:__ Dieses Ergänzungsszenario weist eine etwas schwächere Elektrifizierung auf als das Basisszenario. Stattdessen spielen Bio- und synthetische Gase eine grössere Rolle.
- __ZERO-Variante C:__ Dieses Ergänzungsszenario weist eine etwas schwächere Elektrifizierung auf als das Basisszenario. Stattdessen spielen Wärmenetze, biogene Treibstoffe und Synfuels eine stärkere Rolle.
- __Vergleichsszenario "Weiter wie bisher" (WWB):__ Dieses Szenario dient zum Vergleich mit den ZERO-Szenarien. Darin wurde der Trend aus der jüngeren Vergangenheit von der aktuellen Situation (ab 2018) aus in die Zukunft extrapoliert. Das heisst, dass mit stetig wachsender Bevölkerung und einem generellen Anstieg des Energieverbrauchs aufgrund der Zunahme von Mehrfachaustattungen (Zweitgeräte etc.) und weiteren zusätzlichen strombenötigenden Geräten zur Komfortsteigerung gerechnet wird. Dies bei Berücksichtigung von derzeit bestehenden, in Kraft getretenen energiepolitischen Instrumenten und aktuellen Marktbedingungen. Neuartige, zukünftige und derzeit noch nicht in Kraft getretene Instrumente oder neuartige Gegebenheiten am Markt wurden absichtlich nicht berücksichtigt. Des Weiteren spielt in diesem Szenario eine deutlich sichtbare Einführung von Elektromobilität eine entscheidende Rolle. 


### Entwicklungen
Die vom Bund erstellten Szenarien fussen auf Entwicklungen wichtiger Faktoren für ein zukünftiges Energiesystem. Folgende Punkte hat der Bund dabei berücksichtigt:
- __Bevölkerung:__ Es wird angenommen, dass die Schweizer Bevölkerung bis 2050 auf 10.3 Millionen Einwohner ansteigt.
- __Wirtschaft:__ Das Bruttoinlandprodukt (BIP) steigt im Vergleich zu heute (2019) um 38 % an.
- __Verkehrsentwicklung:__ Die Personenkilometer (km/Pers.) nehmen bis 2050 im Vergleich zu heute (2019) um ca. 17 % zu. Im Güterverkehr nehmen die Tonnenkilometer (km/t) sogar um ca. 31 % zu.
- __Energiebezugsflächen:__ Flächen, welche etwa zum Heizen oder Klimatisieren einen Energiebedarf aufweisen, nehmen gegenüber heute (2019) um rund 17 % zu.
- __Klima:__ In den ZERO-Szenarien nehmen die Heizgradtage (Tage, an denen geheizt werden muss) im Vergleich zum Mittel von 2000 - 2019 um 6 % ab. Dafür nehmen die Kühlgradtage (Tage, an denen gekühlt werden muss) um 18 % zu. Im WWB-Szenario wiederum nehmen die Heizgradtage um 9 % ab und die Kühlgradtage nehmen deutlich um 48 % zu. 
- __International:__ Im ZERO-Szenario wird davon ausgegangen, dass alle Länder, welche dem Übereinkommen von Paris zugestimmt haben, gleich ambitionierte Ziele verfolgen, wodurch die Verlagerung von Emissionen ins Ausland keine Option darstellt. Stattdessen kann von einem schnelleren technologischen Fortschritt profitiert werden.

## Herausforderungen
Eine Anpassung des Energiesystems an eine emissionsfreie Zukunft bringt eine Reihe von Herausforderungen mit sich. Ganz grob zusammengefasst sind dies: 
- Der wegfallende Atomstrom muss erneuerbar erzeugt werden. Wasserkraft hat aber sein Limit schon jetzt erreicht, ist also keine Option. Die Lücke wird also mehrheitlich die Solarenergie füllen müssen.
- Windkraft, Biomasse und andere (thermischen) erneuerbaren Quellen werden den Zusatzbedarf decken.

Konkret in Zahlen muss folgende Entwicklung angestrebt werden:
- Zurzeit produziert die Schweiz ca. 68 TWh Strom jährlich. Davon sind ca. 25 TWh aus Kernkraftwerken, die es zu ersetzen gilt. Für 2050 wird ein jährlicher Energiebedarf von 70 - 80 TWh prognostiert. Im Mittel werden also ca. 30 TWh zusätzlicher Strom aus erneuerbaren Quellen benötigt. Für den Zubau bleiben noch ca. 30 Jahre, was 1 TWh Ausbau pro Jahr verlangt. 
- Wenn der Zusatzbedarf zu zwei Dritteln aus Solarenergie bezogen werden sollte, sind das zusätzlich 20 TWh Solarstrom bis 2050, oder 0.66 TWh jedes Jahr.

Verlangt wäre also ein Ausbau mit 1 TWh aller erneuerbaren Stromerzeugern in jedem kommenden Jahr. Tatsächlich beträgt das derzeitige jährliche Wachstum aber lediglich 0.34 TWh in den vergangenen fünf Jahren. Mit dieser dreimal zu tiefen Zuwachsrate dauert der Umbau der Schweizer Energieversorgung bis ins Jahr 2110. Doch was sind die Gründe dafür?
- __Solarenergie:__ Grundsätzlich besteht für die Solarenergie ein grosses Potenzial. Das sieht auch das BFE so. Es hat berechnet, dass bei Nutzung von relevanten Dachflächen 50 TWh Strom produziert werden könnte. Inklusive relevanter Hausfassaden sogar bis zu 67 TWh. Doch genutzt wird dieses Potenzial nicht so wie es sollte. Der Hauptgrund ist der fehlende ökonomische Anreiz für Solaranlagen. Neue Fördermittel sollen dies ändern.
- __Windenergie:__ Windenergie wäre eine sinnvolle Ergänzung zur Solarenergie, da diese umgekehrte saisonale Effekte aufweist. Während die Solarenergie vor allem im Sommer viel Strom erzeugt, lässt sich mit Windenergieanlagen im Winter etwa doppelt so viel Strom erzeugen wie im Sommer. Es gibt also einen guten Grund für den Ausbau der Windenergie. Die derzeitige Situation zeichnet aber ein anderes Bild. In der Energiestrategie 2050 sind 4 TWh an produziertem Windstrom vorgsehen. Heute liefern die derzeit bestehenden Anlagen nur 0.12 TWh, also noch 33-mal zu wenig. Während beispielsweise in Österreich ca. 1300 Anlagen stehen, sind es in der Schweiz lediglich 65. Hauptgrund für diesen viel zu spärlichen Ausbau sind langwierige Bewilligungsverfahren und Einsprachen, welche dafür sorgen, dass sich Projektrealisierungen über viele Jahre hinziehen.
- __Thermische Energie:__ Wenn thermische Energie als Energiequelle für die Erzeugung von Strom dient, dann ist aktuell bei 62 % davon Kehricht der Energieträger. Schweizer Kehrichtverbrennungsanlagen (KVAs) erzeugen diesen Strom über Dampfturbinen aus der Verbrennungsabwärme. Insgesamt ist dies bei 30 Schweizer KVAs der Fall. Die restliche elektrische Energie stammt von Fernheizkraftwerken und Gross- und Klein-Wärme-Kraftkopplungsanlagen aus der Industrie, welche vor allem auch für die Erzeugung von Wärme betrieben werden. In dieser Kategorie sind die Energieträger zu 16 % Erdgas, zu 10 % Bio- und Klärgas und zu 7 % Holz. Das ist allerdings keine zukunftsorientierte Energieträger-Zusammenstellung. Während Holz, Bio- und Klärgas einen geschlossenen Kohlenstoffkreislauf ermöglichen, gilt es Erdgas zu ersetzen. Dies vorallem durch einen Ausbau der Biomassevergärung zur Erzeugung von Biogas. Gemäss Energiestrategie 2050 vom Bund sollten auf thermischem Weg rund 5 TWh Strom erzeugt werden. Derzeit sind wir bei ca. 3.8 TWh, wobei es neben dem Ausbau zusätzlich noch das bereits erwähnte Erdgas als Energieträger zu ersetzen gilt.
- __Geothermie:__ Das Prinzip der Geothermie wäre an sich äusserst sinnvoll zur Erzeugung von Strom. Wasser wird 4 - 5 km tief in heisse Gesteinsschichten gepumpt, wo es erhitzt wird und mit ca. 150 °C wieder an die Erdoberfläche zurückgeholt wird. Daraus kann über Dampfturbinen Strom erzeugt werden. Dies jederzeit und saisonal unabhängig. Deshalb prognostiziert der Bund in seiner Energiestrategie 2050 ein Potenzial von 4 TWh an geothermisch erzeugtem Strom für 2050. Doch Vision und Realität weichen bei Geothermie besonders stark voneinander ab. Derzeit ist kein einziges Geothermiekraftwerk in Betrieb. An etwa einem Duzend Standorten wurden Abklärungen und Probebohrungen durchgeführt. Weiter fortgefahren wurde aber selten. Dies hat vor allem mit der Probebohrung in Basel BS zu tun, welche im Jahr 2006 nach einem nicht unerheblichen Erdbeben zum Stillstand vieler Geothermieprojekte geführt hat. Das derzeit am weitesten fortgeschrittene Projekt liegt im Kanton Jura und hat die Hoffnungen neu geweckt. Sollte die Geothermie dadurch neu zum Leben erweckt werden können, ist ab ca. 2035 mit relevanten Beiträgen zur Schweizer Energieversorgung zu rechnen.   
__Geothermie im PowerCheck:__ Da noch keine stromproduzierenden Kraftwerke vorhanden sind, existiert auch keine Erzeugerkategorie "Geothermie". Wenn dennoch Geothermiekraftwerke mitberücksichtigt werden sollen, können diese als Zusatzerzeuger manuell hinzugefügt werden, oder sie werden beispielsweise der Erzeugerkategorie "Thermische Kraftwerke" zugerechnet.
- __Effizienzzuwachs auf Verbraucherseite:__ Die kommende Einführung der Elektromobilität verschiebt den Energiebedarf der Mobilität, welcher bis anhin durch chemische Energieträger wie Benzin oder Diesel bereitgestellt wurde, ins Stromnetz. Diesen Zusatzbedarf an elektrischem Strom gilt es abzudämpfen, damit die relativ tiefen prognostizierten 10 % an Mehrbedarf bis 2050 gehalten werden können. Das soll durch Effizienzsteigerungen bei Stromverbrauchern möglich sein, wo teilweise grosse Einsparpotenziale möglich sind. Beispiele sind hier: 
	- Konsequenter Einsatz von sparsamen LED-Leuchtmitteln
	- Ersatz von Elektro-Widerstandsheizungen durch optimal evaluierte Wärmepumpen mit hohen Leistungszahlen (COP) in Kombination mit verbesserter Gebäudeisolation
	- Effizienteres Nutzen der kommenden Elektromobilität, beispielsweise mit Car-Sharing-Prinzipien. 
    
    All das sind Möglichkeiten zur Erhöhung der Effizienz ohne Komforteinbussen. Bei gleichzeitiger Kombination mit einem generell sparsameren Umgang  mit Energie, ist eine erneuerbare Energieversorgung möglich in der Schweiz. 

## Potenziale

###### Tabelle 8.1: Ausschöpfbare Potenziale der erneuerbaren Stromproduktionsanlagen für die Schweiz. Quellen: Potenziale, Kosten und Umweltauswirkungen von Stromproduktionsanlagen (Synthese) - Paul Scherrer Institut (PSI) im Auftrag des BFE 2017; Aufdatierung des Hauptberichts - Paul Scherrer Institut (PSI) im Auftrag des BFE 2019
| Technologie                       |      Jahresenergieertrag 2020 (TWh) | Jahresenergieertrag 2035 (TWh) | Jahresenergieertrag 2050 (TWh) |
| :-------------------------------- | :---------------------------------: |:------------------------------:|:-----------------------------: |
| Grosswasserkraft                  |                                 ~32 |                   32.5 - 34.2* |                   32.5 - 33.6* |
| Kleinwasserkraft                  |                                 4.0 |                    ~4.0 - 4.4* |                    ~4.0 - 4.4* |
| Windenergie                       |                                 0.1 |                      0.7 - 1.7 |                      1.4 - 4.3 |
| Photovoltaik (Dach & Fassaden)    |                                1.68 |                     24.6 & 5.6 |                 (22 - 54) & 17 |
| Holz-BHKW                         |                                 0.1 |                      0.1 - 0.6 |                      0.1 - 1.1 | 
| Landwirtschaftliche Biogasanlagen |                                 0.1 |                      0.1 - 0.7 |                      0.1 - 1.3 |
| Tiefengeothermie                  |               nicht vorhanden |  voraussichtlich noch nicht in grossem Massstab verfügbar |4.5 (Ziel) |

*Da ein zukünftiges Gewässerschutzgesetz die Wasserkraftnutzung tendenziell strenger reglementieren wird, geht das PSI von einem schwächeren Wachstum, resp. sogar von einem Rückgang des Jahresenergieertrages zwischen 2035 und 2050 aus.

	
Textquellen:
- https://www.bfe.admin.ch/bfe/de/home/politik/energiestrategie-2050/dokumentation/energieperspektiven-2035.html
- https://www.bfe.admin.ch/bfe/de/home/politik/energiestrategie-2050/dokumentation/energieperspektiven-2050.html
- https://www.newsd.admin.ch/newsd/message/attachments/64101.pdf
- https://www.bfe.admin.ch/bfe/de/home/news-und-medien/medienmitteilungen/mm-test.msg-id-74641.html
- https://www.bfe.admin.ch/bfe/de/home/news-und-medien/medienmitteilungen/mm-test.msg-id-81356.html
- Energiewende? Ja, die Schweiz schafft das – Republik vom 05.11.2020  