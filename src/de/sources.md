# 9. Daten und Quellen

## Historische Energieproduktions- und Verbrauchsdaten

### Endenergieverbrauch

Die Endverbrauchsdaten in 15-minütiger Auflösung stammen von Swissgrid. Endverbrauch bedeutet, die bei den Konsumenten verbrauchte Energie. Die Übertragungsverluste sind darin nicht enthalten, ebensowenig der Eigenverbrauch der Energieerzeuger.

Die Zeitreihen von Swissgrid ( Excel: Energieübersicht Schweiz, Spalte: Summe endverbrauchte Energie Regelblock Schweiz ) werden mit einem Faktor von durchschnittlich ungefähr 1.02 multipliziert, damit sie mit den Jahresbilanzen der Schweizerischen Elektrizitätsstatistik ( Tabelle: Elektrizitätsbilanz der Schweiz ( Kalenderjahr ), Spalte: Endenergie ) übereinstimmen.

- [Energieübersicht Schweiz ( Excel, Swissgrid )](https://www.swissgrid.ch/de/home/operation/grid-data/generation.html)
- [Schweizerische Elektrizitätsstatistik ( PDF, Bundesamt für Energie )](https://www.bfe.admin.ch/bfe/de/home/versorgung/statistik-und-geodaten/energiestatistiken/elektrizitaetsstatistik.html/) 


### Kernenergie

Die monatlichen Stromerträge der Kernkraftwerke finden sich im Dokument "Gesamte Erzeugung und Abgabe elektrischer Energie in der Schweiz" des Bundesamts für Energie. Diese monatlichen Erträge dienen als Grundlage für die hinterlegten Daten für Kernenergie. Um die Auflösung etwas zu verfeinern, wird zusätzlich der "Wochenbericht zur Erzeugung und Abgabe elektrischer Energie in der Schweiz" des BFE verwendet. Die hier enthaltenen Tageserträge jeweils für Mittwoch werden für die ganze Woche verwendet. Diese Wochenwerte werden dann etwas skaliert, so dass sie mit den erwähnten Monatserträgen übereinstimmen.

- [Gesamte Erzeugung und Abgabe elektrischer Energie in der Schweiz ( PDF, Bundesamt für Energie )](https://www.bfe.admin.ch/bfe/de/home/versorgung/statistik-und-geodaten/energiestatistiken/elektrizitaetsstatistik.html/)
- [Wochenstatistik Elektrizitätsbilanz. Erzeugung und Abgabe elektrischer Energie in der Schweiz ( PDF, Bundesamt für Energie )](https://www.bfe.admin.ch/bfe/de/home/versorgung/statistik-und-geodaten/energiestatistiken/elektrizitaetsstatistik.html/)



## Zuflüsse Speicherseen

Die monatlichen Speicherstände der aggregierten Speicherseen finden sich im Dokument "Gesamte Erzeugung und Abgabe elektrischer Energie in der Schweiz" des BFE. Bei diesen Speicherständen handelt es sich um die nutzbare Energie, die in den Seen gespeichert ist (also die potentielle Energie des gespeicherten Wassers mal den Wirkungsgrad Wasserkraftwerke). Unter Berücksichtigung der monatlichen Stromproduktionsmengen der Wasserkraftwerke aus demselben Dokument wird auf die monatliche Zuflussmenge der Speicherseen geschlossen. Dabei muss berücksichtigt werden, dass in dieser Statistik in der Stromproduktionsmengen der Wasserkraftwerke auch die Stromproduktion von Pumpspeicherwerken enthalten ist.
Um die monatliche Auflösung zu verfeinern, wird zusätzlich der "Wochenbericht des Speicherinhalts" des BFE verwendet. So können wöchentliche Variationen in der Zuflussmenge berücksichtigt werden.

- [Gesamte Erzeugung und Abgabe elektrischer Energie in der Schweiz ( PDF, Bundesamt für Energie )](https://www.bfe.admin.ch/bfe/de/home/versorgung/statistik-und-geodaten/energiestatistiken/elektrizitaetsstatistik.html/)
- [Wochenbericht Füllungsgrad der Speicherseen" (PDF, Bundesamt für Energie )](https://www.bfe.admin.ch/bfe/de/home/versorgung/statistik-und-geodaten/energiestatistiken/elektrizitaetsstatistik.html/)

###### Abbildung 9.1: Berechnete natürliche Speichersee-Zuflüsse (nutzbare Leistung) der vergangenen Jahre und Durchschnittskurve. Zusätzlich dargestellt ist die Speicherkapazität aller Speicherseen.
![Zuflüsse der Speicherseen](../img/inflow.svg)

## Historische Wetterdaten

