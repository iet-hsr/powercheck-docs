---
sidebarDepth: 2
---
# 2. Bedienungsanleitung

## Benutzeroberfläche

Die Benutzeroberfläche ist in sieben Bereiche unterteilt, wie unten dargestellt.

1. Hauptmenü. Hier können Szenarien geladen oder gespeichert werden.
2. Szenariendefinition. Hier können Szenarien definiert werden.
3. Titel und Beschreibung des aktuell berechneten Szenarios.
4. Funktionalität zum Vergleich von Szenarien.
5. Kurzzusammenfassung des aktuell simulierten Szenarios.
6. Grafiken zum aktuell simulierten Szenario.
7. Kennzahlen zum aktuell simulierten Szenario.

###### Abbildung 2.1: Benutzeroberfläche.
![Benutzeroberfläche](../img/gui.png)

Ein Klick auf (1.) oder (2.) öffnet die Benutzeroberfläche für die Szenariendefinition (Abb. 2.2).

1. Hier lassen sich Szenarien laden, speichern oder löschen. Der Startknopf für die Simulation befindet sich ebenfalls hier.
2. Hier können die einzelnen Stromverbraucher oder Kraftwerke definiert werden.

###### Abbildung 2.2: Benutzeroberfläche Szenariendefinition.
![Benutzeroberfläche Szenariendefinition](../img/gui_sidebar.png)

## Laden, speichern oder löschen von Szenarien

Es können vordefinierte Szenarien geladen werden, indem ein Szenario aus der Dropdown-Liste ausgewählt wird. Die Einstellungen (z.B. Menge des Stromverbrauchs) in einem Szenario können verändert werden. Dieses neue Szenario kann auf dem lokalen Computer des Benutzers oder der Benutzerin abgespeichert werden. Das Dateiformat ist .json. Die Datei kann mit einem Texteditor geöffnet und allenfalls auch angepasst werden. Hier ein Ausschnitt aus einer .json Datei:

```
{
  "name": "myScenario",
  "parameter": {
    "version": 6,
    "endUser": {
      "year": 2019,
      "scaling": 57198,
      "additionalEMobilityEnergy": 0
    },
    ...
```

## Start der Simulation

Der Startknopf für die Simulation befindet sich im ausgelappten roten Feld oben links (Abb. 2.2). Die Simulation wird im aktuell geöffneten Browser berechnet. Je nach Browser und Leistungsfähigkeit des Computers dauert sie etwas mehr oder weniger lang, in der Regel ein paar Sekunden.


## Definition eines Szenarios

Die Idee hinter den Szenarien ist, dass jeweils alle Kraftwerke eines Typs zu einem einzigen grossen Kraftwerk "aggregiert" (zusammengefasst) werden. Die Eigenschaften dieser aggregierten Kraftwerke können dann im Bereich (2.) von Abb. 2.2 eingestellt werden. Es lassen sich jeweils viertelstündlich aufgelöste historische Daten (z.B. Stromverbrauch im Jahr 2019) verwenden, die mit dem Schieberegler oder dem Zahlenfeld beliebig skaliert werden können.

### Stromverbrauch (Endverbrauch)

Hier lässt sich der Stromverbrauch der Schweiz ("Endverbrauch", ohne Netzverluste) einstellen. In der Vergangenheit setzte sich dieser zusammen aus ca. je einem Drittel für Haushalte, Industrie und Dienstleistungen, sowie einem kleinen Teil für Verkehr. Der Stromverbrauch bewegte sich in den letzten zehn Jahren stabil zwischen 55 und 60 TWh. Das BFE rechnet in seinen "Energieperspektiven 2050+" mit einem jährlichen Endverbrauch (inkl. Elektromobilität und Wärmepumpen) im Jahr 2050 zwischen 58 und 67 TWh.

Für das Szenario kann ein historisches Stromverbrauchsprofil (Profil sichtbar in Abb. 2.2) in Viertelstundenauflösung durch Anwählen eines Jahres ausgewählt werden. Über den Schieberegler oder das Zahlenfeld kann dieses Profil skaliert werden, dabei bleibt die Form des Profils bestehen.

Zudem kann eingestellt werden, welcher Anteil des heutigen Strassenverkehrs rein elektrisch betrieben werden soll. Die zur Ladung der Elektrofahrzeuge nötige Energie wird gleichmässig über das ganze Jahr zum eingegebenen Stromverbrauch dazugerechnet. Als Basis dazu dient der momentan (2020) jährliche Energiebedarf in Form von Benzin und Diesel von 82 TWh (unterer Heizwert).

[Mehr zu dieser Umrechnung...](calculation-methodology.md#elektrische-energie-fur-den-verkehr)

Soll der zusätzliche Strombedarf durch Elektrofahrzeuge zeitlich anders verteilt werden, kann ein benutzerdefiniertes Profil hochgeladen werden. Das Gleiche gilt für den Fall, dass zusätzliche Wärmepumpen für Gebäudeheizungen mitberücksichtigt werden wollen.


### Kernkraftwerke

Hier lässt sich die Stromerzeugung aus Kernkraftwerken einstellen. Für die Jahre 2014 bis 2019 ist die Summe der Stromerzeugung aller Schweizer Kernkraftwerke hinterlegt. Durch Anwählen eines Jahres wird das entsprechende Jahresprofil der Stromerzeugung aus Kernkraft ausgewählt. Über den Schieberegler oder das Zahlenfeld kann die installierte Leistung skaliert werden. Dabei bleibt die Form des Profils bestehen.

[Mehr über Kernkraftwerk](producers-consumers.md#kernkraftwerke)

### Thermische Kraftwerke

Hier lässt sich die Stromerzeugung aus thermischen Kraftwerken einstellen. Für die letzten paar Jahre ist die Summe der Stromerzeugung aller thermischen Kraftwerke in der Schweiz hinterlegt. Durch Anwählen eines Jahres wird das entsprechende Jahresprofil der thermischen Stromerzeugung ausgewählt. Über den Schieberegler oder das Zahlenfeld kann die installierte Leistung auch manuell geändert werden. Dabei bleibt die Form des Profils bestehen.

[Mehr über thermische Kraftwerke](producers-consumers.md#thermische-kraftwerke)

### Solarenergie

Hier lässt sich die installierte Leistung aller Solaranlagen (Photovoltaik) in der Schweiz einstellen. Diese Leistung wird auch Nennleistung oder kWp (Kilowatt-Peak) genannt und muss hier in der Einheit Gigawatt eingegeben werden. Für die letzten paar Jahre sind Strahlungsdaten von insgesamt 20 Messstationen in der Schweiz hinterlegt. Durch Anwählen eines Jahres wird das entsprechende Jahresprofil der Sonneneinstrahlung ausgewählt. Das  Programm rechnet diese Strahlungsdaten unter Berücksichtigung der installierten Leistung und der Verteilung der Ausrichtungen (Himmelsrichtung und Neigung) in elektrische Leistungen um. Über den Schieberegler oder das Zahlenfeld kann die installierte Leistung geändert werden. Dabei bleibt die Form des Profils bestehen. Die Gewichtung der 20 Messstationen sowie die Verteilung der Ausrichtungen können in den Expertenparameter definiert werden.

[Mehr über Solarenergie](producers-consumers.md#solarenergie)

### Windenergie

Hier lässt sich die installierte Leistung aller Windkraftanlagen in der Schweiz einstellen. Für die letzten paar Jahre sind Windgeschwindigkeitsdaten von insgesamt 18 Messstationen in der Schweiz hinterlegt. Durch Anwählen eines Jahres wird das entsprechende Jahresprofil der Windgeschwindigkeiten ausgewählt. Das  Programm rechnet die Windgeschwindigkeitsdaten unter Berücksichtigung der installierten Leistung in elektrische Leistungen um. Über den Schieberegler oder das Zahlenfeld kann die installierte Leistung  auch manuell geändert werden. Dabei bleibt die Form des Profils bestehen.

[Mehr über Windenergie](producers-consumers.md#windenergie)

### Flusskraftwerke

Hier lässt sich die Stromerzeugung aus Flusslaufkraftwerken einstellen. Für die Jahre 2014 bis 2019 ist die Summe der Stromerzeugung aller Flusskraftwerke in der Schweiz hinterlegt. Durch Anwählen eines Jahres wird das entsprechende Jahresprofil der Stromerzeugung aus Flusslaufkraft ausgewählt. Über den Schieberegler oder das Zahlenfeld kann die installierte Leistung  auch manuell geändert werden. Dabei bleibt die Form des Profils bestehen.

[Mehr über Flusskraftwerke](producers-consumers.md#flusskraftwerke)


### Stauseen

Hier lässt sich die Stromerzeugung aus Stauseekraftwerken einstellen. Für die Jahre 2014 bis 2019 ist die Summe aller natürlichen Zuflüsse in Stauseen in der Schweiz hinterlegt. Es gilt zu beachten, dass hier nur Stauseen mit natürlichen Zuflüssen ohne Pumpfunktion berücksichtigt werden. Durch Anwählen eines Jahres wird das entsprechende Jahresprofil der natürlichen Zuflüsse in die Stauseen eingestellt. Natürliche Zuflüsse stammen von Niederschlag und Schneeschmelze.

Folgende Werte können mittels Schieberegler oder Zahleneingabe definiert werden:
- Nennleistung Turbinen (maximale Stromabgabeleistung der Turbinen)
- Füllstand am 1. Januar 00:00 Uhr (nutzbare Energie)
- Kapazität aller Schweizer Stauseen (nutzbare Energie)

[Mehr über Stauseen](storage.md#stauseen)

### Pumpspeicherkraftwerke

Hier lässt sich die Stromerzeugung aus und Speicherung in Pumpspeicherkraftwerken einstellen. Es gilt zu beachten, dass hier nur Stauseen mit künstlichen Zuflüssen mittels Pumpen berücksichtigt werden.

Folgende Werte können mittels Schieberegler oder Zahleneingabe definiert werden:
- Nennleistung Pumpen (maximale Stromaufnahmeleistung der Pumpen)
- Nennleistung Turbinen (maximale Stromabgabeleistung der Turbinen)
- Füllstand am 1. Januar 00:00 Uhr (nutzbare Energie)
- Kapazität aller Pumpspeicherkraftwerke (nutzbare Energie)

Falls zum Füllstand am 1. Januar keine Angaben vorliegen, kann der Wert auf null eingestellt werden. Diese Einstellung hat kaum einen kleinen Einfluss auf die gesamte Simulation.

[Mehr über Pumpspeicherkraftwerke](storage.md#pumpspeicherkraftwerke)

### Batteriespeicher

Hier lässt sich die Stromerzeugung aus und Speicherung in Batteriespeichern einstellen.

Folgende Werte können mittels Schieberegler oder Zahleneingabe definiert werden:
- Minimale Ladezeit (wird in maximale Ladeleistung umgerechnet)
- Minimale Entladezeit (wird in maximale Entladeleistung umgerechnet)
- Füllstand am 1. Januar 00:00 Uhr (nutzbare Energie)
- Kapazität des Speichers (nutzbare Energie)

Falls zum Füllstand am 1. Januar keine Angaben vorliegen, kann der Wert auf null eingestellt werden. Diese Einstellung hat kaum einen kleinen Einfluss auf die gesamte Simulation.

[Mehr über Batteriespeicher](storage.md#batteriespeicher)

### Benutzerdefiniert

Hier können eigene (benutzerdefinierte) Stromverbrauchs- und/oder -produktionsprofile hochgeladen werden. Auf diese Weise lassen sich zusätzliche Stromverbraucher oder -erzeuger in der Berechnung  berücksichtigen. Die Leistungsprofile werden nach erfolgreichem Upload als Diagramm dargestellt. Das Profil kann über Schieberegler oder Zahlenfelder skaliert werden.

#### Datenformat

Die Leistungsdaten müssen in folgender Form hochgeladen werden:

- Dateiendung: .csv
- 1 Spalte
- 35'040 Zeilen (für jede Viertelstunde einen Leistungswert in der Einheit Kilowatt über die Dauer von 365 Tagen)
- 1. Zeile: Leistungsdurchschnitt für das Zeitintervall 1. Januar 00:00 bis 00:15 Uhr, usw.

#### Beispieldatei

Die Beispieldatei beinhaltet Daten eines zufällig verlaufenden Leistungsprofils mit einem Mittelwert von ca. 51 kW. Es dient primär zur Veranschaulichung der geforderten Datenformatierung.

### Expertenparameter

#### Solarenergie

**Ausrichtung:** Hier lässt sich anhand einer variablen Häufigkeitsverteilung einstellen, wie die in der Schweiz installierten Solarzellen ausgerichtet sind. Hierfür sind Azimut- und Neigungsverteilung an vordefinierten Richtungen einstellbar. Standardmässig ist der Azimut in Südrichtung normalverteilt und die Neigung am 15°-Winkel verschoben normalverteilt.

**Standortgewichtung:** Hier lässt sich eine von drei vordefinierten Standortgewichtungen einstellen. Standardmässig wird anhand der Bevölkerungsdichte der Regionen gewichtet.

#### Speicherwasserkraftwerke

**Wirkungsgrade:** Hier lassen sich der Entlade- und Ladungswirkungsgrad aller schweizweit installierten Speicherwasserkraftwerke (Stausee- und Pumpspeicherkraftwerke) einstellen. Standardmässig betragen der Entladewirkungsgrad 90 % und der Ladewirkungsgrad 85 %.

**Stauseekraftwerke:** Hier setzt sich der Entladewirkungsgrad zusammen aus dem  Turbinenwirkungsgrad und dem Generatorwirkungsgrad. Es kommen zumeist Pelton- oder Francisturbinen mit Wirkungsgraden von ca. 92 % zum Einsatz. Grosse Generatoren zur Stromerzeugung weisen Wirkungsgrade von ca. 98 % auf.

**Pumpspeicherkraftwerke:** Moderne Pumpspeicherkraftwerke wie z.B.  Linth-Limmern sind mit Francis-Pumpturbinen und Motor-Generatoren  ausgestattet, welche in beide Richtungen nutzbar sind. Im  Turbinenbetrieb arbeiten Francis-Turbinen mit einem Wirkungsgrad von ca. 92 % und im Pumpbetrieb mit einem Wirkungsgrad von ca. 90 %. Der Wirkungsgrad moderner Motor-Generatoren beträgt in beide Nutzungsrichtungen ca. 98 %.

Quelle: Pöyry Energy AG im Auftrag des BFE - Bestimmung von Wirkungsgraden bei Pumpspeicherung in Wasserkraftanlagen (2008)  

#### Batteriespeicher

**Wirkungsgrade:** Hier lassen sich der Entlade- und Ladungswirkungsgrad aller schweizweit installierten Batteriespeicher einstellen. Standardmässig betragen der Lade- und Entladewirkungsgrad 90 %.

#### Stromnetz

**Netzverluste:** Hier lassen sich die Netzverluste einstellen, wobei "Powercheck" nur eine Netzebene berücksichtigt. Standardmässig beträgt der Netzverlust 7 %. 