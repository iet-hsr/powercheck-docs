# 4. Berechnungsmethodik

## 4.1 Leistungsbilanz

Im Zentrum der Simulation steht die Leistungsbilanz. Die Abbildung unten zeigt schematisch die Leistungsflüsse ins und aus dem Schweizer Stromnetz. Die Leistungsbilanz muss jederzeit ausgeglichen sein. Das heisst, die Summe der einfliessenden Leistungen ( $p$ für production ) muss gleich der Summe der ausfliessenden Leistungen ( $c$ für consumption ) sein.

$$\sum \limits_i \boldsymbol{P}_{\textrm{p},i} - \sum \limits_j \boldsymbol{P}_{\textrm{c},j} =0$$

> $\boldsymbol{P}$ kann hier und weiter unten als momentane Leistung $P(t)$ oder als Vektor mit den 35'040 15-Minuten-Durchschnittsleistungen eines Jahres interpretiert werden.

$i$ steht für die verschiedenen Energieproduzenten und $j$ für die Energiekonsumenten, wie in nachfolgender Tabelle dargestellt.

Die Leistungsprofile der nicht regelbaren Stromerzeuger ergeben sich aus den historischen Stromerzeugungs- und Wetterdaten.

###### Tabelle 4.1: Stromerzeuger und -verbraucher. Die nicht-regelbaren Erzeuger oder Verbraucher basieren auf historischen Daten.

| $i$   | $j$  | Beschreibung                       | Regelbarkeit | Hist. Produktions- und Verbrauchsdaten | Hist. Wetterdaten |
| ----- | ---- | ---------------------------------- | ------------ | -------------------------------------- | ----------------- |
|       | dem  | Stromverbrauch ( ohne Verluste )     | Nein         | Ja                                     |                   |
| therm |      | Thermische Kraftwerke              | Nein         | Ja                                     |                   |
| nucl  |      | Atomkraftwerke                     | Nein         | Ja                                     |                   |
| riv   |      | Flusskraftwerke                    | Nein         | Ja                                     |                   |
| wind  |      | Windkraftwerke                     | Nein         |                                        | Wind              |
| sol   |      | Photovoltaikanlagen                | Nein         |                                        | Sonne             |
| ud    | ud   | Benutzerdefinierte Stromproduktion | Nein         |                                        |                   |
| lake  |      | Wasserkraftwerke ( Stausee )         | Ja           |                                        | Niederschlag      |
| pump  | pump | Pumpspeicherwerke                  | Ja           |                                        |                   |
| batt  | batt | Strom aus Batteriespeichern        | Ja           |                                        |                   |
| imp   | exp  | Stromimport                        | Ja           |                                        |                   |
|       | loss | Verluste                           |              |                                        |                   |


###### Abbildung 4.1: Schematische Darstellung der Leistungszu- und -abflüsse ins elektrische Netz.

![Energy Balance](../img/energy_balance.svg)


## 4.2 Grundlegende Strategie

Es gibt beliebig viele Möglichkeiten, wie eine vorgegebene Stromnachfrage durch die verschiedenen Kraftwerkstypen und Energiespeicher gedeckt werden kann. Darum muss eine Gesetzmässigkeit ( Strategie ) definiert werden, die eindeutig festlegt, wann welcher Kraftwerks- oder Speichertyp zum Einsatz kommt.

Die verwendete Strategie für das Simulationsprogramm *powerCheck* basiert auf dem Buch Kraftwerk Schweiz von Anton Gunzinger. Sie umfasst nur zwei Punkte:

1. Es wird möglichst hohe Stromautarkie für die Schweiz angestrebt ( es wird möglichst versucht, den momentanen Leistungsbedarf mit inländischen Kraftwerken zu decken ).
2. Es werden keine Kraftwerke abgeregelt ( es wird kein Strom «verworfen» ). 

Diese Strategie entspricht nicht der heutigen Realität. Für die Kraftwerksbetreiber stehen kommerzielle Interessen im Vordergrund. Die Simulationsergebnisse können deshalb von der Ist-Situation abweichen, insbesondere wird in Realität mehr Strom importiert und exportiert als im Simulationsmodell. Die beschriebene Strategie für das Modell wird gewählt, weil es eindeutig und klar ist, und weil das Modell aufzeigen soll, zu welchem Grad die Schweiz selber ihren Strombedarf decken kann.

## 4.3 Berechnung der regelbaren Leistungen

Aus der oben beschriebenen Strategie ergeben sich folgende Rechenregeln:

1.	Der Stromendverbrauch sowie Kraftwerke, die nicht regelbar sind, werden zuerst berücksichtigt. Daraus ergibt sich eine Residualleistung $P_\textrm{res}$.

$$P_\textrm{res} = - P_\textrm{c,dem} + P_\textrm{p,term} + P_\textrm{p,nucl} + P_\textrm{p,riv} + P_\textrm{p,wind} + P_\textrm{p,sol} + P_\textrm{p,ud} - P_\textrm{c,ud}$$


2. - a) Falls $P_\textrm{res} > 0$ ( Überproduktion ) werden die Speicher in der Reihenfolge ihrer Effizienz beladen, nämlich

     - i) Beladung der Batteriespeicher, falls möglich
        
     - ii) Beladung der Pumpspeicher, falls möglich

   - b) Falls $P_\textrm{res} < 0$ ( Unterproduktion ) werden die Speicher in der Reihenfolge ihrer Effizienz entladen. Eine Ausnahme bilden die Stauseen, welche mit letzter Priorität entladen werden, weil sie nicht künstlich wieder beladen werden können. Die Reihenfolge der Entladung ist also

     - i) Entladung der Batteriespeicher, falls möglich

     - ii) Entladung der Pumpspeicher, falls möglich

     - iii) Entladung der Stauseen, falls möglich

3.   Falls, unter Berücksichtigung der Beladung oder Entladung der Speicher, immernoch ein Leistungsmangel oder -überschuss besteht, wird dieser durch «Import» oder «Export» gedeckt. 

Mit diesen Rechenregeln können, bei gegebenen Anfangsbedingungen am 1. Januar um 00:00 Uhr, sequentiell Zeitintervall für Zeitintervall ( jeweils 15 Minuten ) alle Leistungsflüsse und die Ladung der Energiespeicher für das ganze Jahr berechnet werden.

###### Abbildung 4.2: Berechnungsschema.

![Berechnungsschema](../img/calculation.svg)


## 4.4 Berechnung des ohmschen Verlusts

Die  Verlustleistung im Übertragungsnetz wird als konstanter Anteil $f$ der in das Netz eingespeisten Leistung ( und somit durch das Netz geleiteten Leistung ) angenommen. Das heisst, es gilt

$$P_\textrm{c,loss} = f \cdot \sum \limits_i P_{\textrm{p},i}$$

Es sei $P_\textrm{p,nc}$ die Leistung aller nicht regelbaren Kraftwerke

$$P_\textrm{p,nc} = P_\textrm{p,term} + P_\textrm{p,nucl} + P_\textrm{p,riv} + P_\textrm{p,wind} + P_\textrm{p,sol} + P_\textrm{p,ud}$$

und $P_\textrm{c,nc}$ die Leistung aller nicht regelbaren Verbraucher

$$P_\textrm{c,nc} = P_\textrm{c,end} + P_\textrm{c,ud}$$

und $P_\textrm{stor}$ die Leistung aus den Energiespeichern ( positiv: laden, negativ: entladen )

$$P_\textrm{stor} = P_\textrm{p,batt} - P_\textrm{c,batt} + P_\textrm{p,pump} - P_\textrm{c,pump} + P_\textrm{p,lake} + P_\textrm{p,imp} - P_\textrm{c,exp}.$$

Die Leistungsbilanz ergibt
$$P_\textrm{p,nc} - P_\textrm{c,nc} + P_\textrm{stor} - P_\textrm{c,loss} = 0.$$

Für die Berechnung der Verlustleistung $P_\textrm{loss}$ muss unterschieden werden, ob die Speicher geladen oder entladen werden.

**Fall 1**: Speicher werden geladen ( $P_\textrm{stor} \le 0$ ):

$$P_\textrm{c,loss} = f \cdot P_\textrm{p,nc}$$

**Fall 2**: Speicher werden entladen ( $P_\textrm{stor} > 0$ ):

$$P_\textrm{c,loss} = f \cdot (P_\textrm{p,nc} + P_\textrm{stor}) = f \cdot (P_\textrm{c,nc} + P_\textrm{c,loss})$$

und damit

$$P_\textrm{c,loss} = \frac{f}{1-f} \cdot P_\textrm{c,nc}$$

## 4.5 Berechnung der nicht-regelbaren Leistungen

### Endverbrauch

Es stehen die historischen Verbrauchsprofile $P_\textrm{c,dem,hist}(y)$ in 15-Minuten-Auflösung der vergangenen Jahre $y$ als Datenbasis zur Verfügung. Die im Jahr $y$ verbrauchte Energie $E_\textrm{c,dem,hist}(y)$ berechnet sich daraus wie folgt:

$$E_\textrm{c,dem,hist}(y) = \overline{\boldsymbol{P}}_\textrm{c,dem,hist}(y) \cdot 8760 \; \textrm{h}$$

Diese Energiemenge kann durch den Benutzer / die Benutzerin angepasst werden ( $E_\textrm{c,dem}$ ). Das historische Verbrauchsprofil aus dem ausgewählten Jahr wird entsprechend skaliert:

$$\boldsymbol{P}_\textrm{c,dem} =\frac{E_\textrm{c,dem}}{E_\textrm{c,dem,hist}(y)} \cdot \boldsymbol{P}_\textrm{c,dem,hist}(y)$$

#### Elektrische Energie für den Verkehr

Zusätzlich kann angegeben werden, welcher Anteil des mit Benzin oder Diesel betriebenen Verkehrs ( Jahr 2019 ) elektrisch angetrieben werden soll. Für die Umrechnung wird der Energieverbrauch eines durchschnittlichen Benzinfahrzeugs ( 6.1 Liter / 100 km ) mit einem vergleichbaren Elektrofahrzeug ( 16 kWh / 100 km ) gegenübergestellt. Daraus ergibt sich das Verhältnis des Energieeinsatzes von 0.271 ( = Energieeinsatz Elektrofahrzeug / Energieeinsatz Benzinfahrzeug, bezogen auf den unteren Heizwert von Benzin ).

Im Jahr 2019 wurden für den gesamten Verkehr in der Schweiz Benzin und Diesel mit einem unteren Heizwert von insgesamt 81'800 GWh. Mit dem oben erwähnten Verhältnis ergibt sich für den rein elektrischen Antrieb ein Energieaufwand von 22'200 GWh.

Dieser Energieaufwand wird mit dem benutzerdefinierten Anteil Elektroverkehr multipliziert und gleichmässig über das ganze Jahr zum Endverbrauchsprofil dazugerechnet.


### Atomkraftwerke, thermische Kraftwerke, Flusskraftwerke

Es stehen die historischen Erzeugungsprofile $\boldsymbol{P}_{\textrm{p,}i\textrm{,hist}}(y)$ in 15-Minuten-Auflösung der vergangenen Jahre $y$ als Datenbasis zur Verfügung.

Die installierte Nennleistung ( «Spitzenleistung» ) kann durch den Benutzer / die Benutzerin angepasst werden ( $P_{\textrm{p,}i\textrm{,max}}$ ). Das historische Verbrauchsprofil aus dem ausgewählten Jahr wird entsprechend skaliert:

$$\boldsymbol{P}_\textrm{c,dem} =\frac{P_{\textrm{p,}i\textrm{,max}}}{\textrm{max}(\boldsymbol{P}_{\textrm{p,}i\textrm{,hist}}(y))} \cdot \boldsymbol{P}_{\textrm{p,}i\textrm{,hist}}(y)$$

mit $i = \textrm{nucl, term, riv}$.

### Windkraftwerke

Für die Berechnung der Leistung aus Windkraftwerken werden Winddaten von zwanzig Standorten in der Schweiz verwendet. Aus den gemessenen Windgeschwindigkeiten und unter Berücksichtigung der benutzerdefinierten installierten Nennleistung aller Windkraftwerke wird das Leistungsprofil über den Zeitraum von einem Jahr berechnet. Dieses Leistungsprofil wird mit effektiv gemessenen Ertragsdaten verglichen, und das Modell mit einem Faktor kalibriert, so dass Modell und Realität für die vergangenen Jahre übereinstimmen.

###### Tabelle 4.2: Liste der Windmessstationen

| Nr.  | Standort          | Höhe ü. M. ( m ) | Messhöhe ( m ) | Latitude ( °N ) | Longitude ( °E ) |
| ---- | ----------------- | :------------: | :----------: | :-----------: | :------------: |
| 1    | La Dôle           |      1670      |      10      |    46.4247    |     6.0994     |
| 2    | Bière             |      684       |      10      |    46.5250    |     6.3425     |
| 3    | La Brévine        |      1050      |      10      |    46.9839    |     6.6103     |
| 4    | Oron              |      828       |      10      |    46.5722    |     6.8583     |
| 5    | La Chaux-de-Fonds |      1017      |      10      |    47.0831    |     6.7922     |
| 6    | Cressier          |      430       |      10      |    47.0475    |     7.0592     |
| 7    | Chasseral         |      1599      |      10      |    47.1317    |     7.0544     |
| 8    | Bantiger          |      942       |     100      |    46.9778    |     7.5286     |
| 9    | Beatenberg        |      1560      |      10      |    46.7006    |     7.7722     |
| 10   | Egolzwil          |      522       |      10      |    47.1794    |     8.0047     |
| 11   | Hildisrieden      |      712       |      10      |    47.1558    |     8.2225     |
| 12   | Schmerikon        |      408       |      10      |    47.2250    |     8.9403     |
| 13   | Hörnli            |      1144      |      10      |    47.3708    |     8.9417     |
| 14   | Chur              |      556       |      10      |    46.8703    |     9.5306     |
| 15   | Arosa             |      1878      |      10      |    46.7925    |     9.6789     |
| 16   | Valbella          |      1568      |      10      |    46.7550    |     9.5544     |
| 17   | Grimsel Hospiz    |      1980      |      10      |    46.5717    |     8.3333     |
| 18   | Leytron           |      512       |      10      |    46.1856    |     7.2211     |

###### Abbildung 4.3: Standorte der Windmessungen. Blaue Regionen: Geeignete Gebiete für Windkraftanlagen.

![Windmessungen](../img/wind_stations.png)


Die Windmessungen erfolgen in der Regel auf einer Höhe von 10 m über Boden ( siehe Tabelle ). Die Nabenhöhe der Windturbinen wird auf 100 m über Boden angenommen. Die Windgeschwindigkeit $v$ hängt vom Abstand $h$ vom Boden ab:

$$\frac{v_2}{v_1} = \frac{\ln(h_2/z_0)}{\ln(h_1/z_0)}$$

mit der Bodenrauigkeit $z_0$. Bei einer Bodenrauigkeit von 0.1 m ergibt sich für die Umrechnung der gemessenen Windgeschwindigkeit auf 10 m Höhe auf die gewünschte Höhe von 100 m ein Geschwindigkeitsverhältnis von 1.5. Aus dieser Windgeschwindigkeit wird mit der Kennlinie eines als repräsentativ erachteten Windkraftwerks die relative Leistung berechnet. Diese sagt aus, mit wieviel Prozent der Nennleistung das Windkraftwerk bei der entsprechenden Windgeschwindigkeit läuft.

###### Abbildung 4.4: Kennlinie einer repräsentativen Windturbine. Bei Windgeschwindigkeiten von über 30 m/s wird die Anlage abgestellt.

![Hier steht die Bildbeschreibung](../img/wind_rel_power.svg)

Die so berechneten jährlichen Ertragsdaten werden mit den effektiv gemessen Erträgen der Schweizer Windkraftwerke verglichen. Der geringe Unterschied wird mit einem Kalibrationsfaktor $f_\textrm{calib,wind} = 1.045$ korrigiert. Das bedeutet, die existierenden Anlagen stehen an geringfügig besseren Standorten ( 4.5 % besser ), als aus dem Durchschnitt der 18 verwendeten Windmessstandorten berechnet wird.

###### Tabelle 4.3: Vergleich von gemessenem und berechnetem Ertrag der Schweizer Windkraftwerke. Die installierte Nennleistung bezieht sich jeweils auf den Jahresdurchschnitt. Die Abweichung im Jahr 2018 ist auf einen längeren Ausfall eines grösseren Windparks zurückzuführen.

| Jahr | Installierte Nennleistung ( MW ) | Volllaststunden gemessen ( h ) | Volllaststunden berechnet ( h ) | Abweichung |
| :--: | :----------------------------: | :--------------------------: | :---------------------------: | :--------: |
| 2019 |              74.9              |             1944             |             1922              |  - 1.1 %   |
| 2018 |              75.2              |             1620             |             1730              |  + 6.8 %   |
| 2017 |              75.2              |             1763             |             1784              |  + 1.1 %   |
| 2016 |              63.0              |             1724             |             1736              |  + 0.7 %   |
| 2015 |              60.3              |             1825             |             1812              |  - 0.7 %   |
| 2014 |              60.3              |             1673             |             1689              |  + 1.0 %   |


### Photovoltaik ( PV )

Die Grundlage für die Berechnung des Ertrags aus Photovoltaik-Anlagen bilden Sonneneinstrahlungsmessdaten von 20 verschiedenen Standorten in der Schweiz. Von diesen Messstationen liegen jeweils die Messungen der direkten Sonneneinstrahlung sowie der diffusen Strahlung auf eine horizontale Platte vor. Weil Photovoltaik-Module nicht unbedingt horizontal montiert werden, müssen diese Einstrahlungsdaten auf die effektive Ausrichtung ( bzw. Ausrichtungsverteilung ) der Module umgerechnet werden.

###### Tabelle 4.4: Liste der Strahlungsmessstationen

| Nr.  | Standort           | Höhe ü. M. ( m ) | Latitude ( °N ) | Longitude ( °E ) | Gewicht «Bevölkerung» | Gewicht «Fläche» | Gewicht «Winterstrom» |
| ---- | ------------------ | :------------: | :-----------: | :------------: | :-------------------: | :--------------: | :-------------------: |
| 1    | Altdorf            |      438       |    46.8869    |     8.6219     |         0.036         |       0.05       |           0           |
| 2    | Andeer             |      987       |    46.6103    |     9.4319     |         0.027         |       0.05       |           0           |
| 3    | Bad Ragaz          |      497       |    47.0167    |     9.5025     |         0.036         |       0.05       |           0           |
| 4    | Bantiger           |      942       |    46.9778    |     7.5286     |         0.072         |       0.05       |           0           |
| 5    | Basel / Binningen  |      316       |    47.5411    |     7.5836     |         0.072         |       0.05       |           0           |
| 6    | Bischofszell       |      470       |    47.4975    |     9.2347     |         0.036         |       0.05       |           0           |
| 7    | Buffalora          |      1968      |    46.6481    |    10.2672     |         0.036         |       0.05       |         0.25          |
| 8    | Evionnaz           |      482       |    46.1831    |     7.0267     |         0.036         |       0.05       |           0           |
| 9    | Fahy               |      596       |    47.4239    |     6.9411     |         0.036         |       0.05       |           0           |
| 10   | Fribourg / Posieux |      651       |    46.7714    |     7.1136     |         0.054         |       0.05       |           0           |
| 11   | Genève / Cointrin  |      411       |    46.2475    |     6.1278     |         0.072         |       0.05       |           0           |
| 12   | Gösgen             |      380       |    47.3631    |     7.9736     |         0.072         |       0.05       |           0           |
| 13   | Grono              |      324       |    46.255     |     9.1639     |         0.036         |       0.05       |           0           |
| 14   | Meiringen          |      589       |    46.7322    |     8.1692     |         0.054         |       0.05       |           0           |
| 15   | Pully              |      456       |    46.5122    |     6.6675     |         0.054         |       0.05       |           0           |
| 16   | Robièi             |      1896      |    46.4431    |     8.5133     |         0.036         |       0.05       |         0.25          |
| 17   | Stabio             |      353       |    45.8433    |     8.9325     |         0.054         |       0.05       |         0.25          |
| 18   | Uetliberg          |      854       |    47.3514    |     8.4903     |         0.09          |       0.05       |           0           |
| 19   | Visp               |      639       |    46.3028    |     7.8431     |         0.054         |       0.05       |           0           |
| 20   | Zermatt            |      1638      |    46.0292    |     7.7525     |         0.036         |       0.05       |         0.25          |

###### Abbildung 4.5: Standorte der Einstrahlungsmessungen.

![Strahlungsmessungen](../img/solar_stations.png)

Photovoltaik wird in *PowerCheck* als nicht regelbar betrachtet, das heisst, es wird immer die gesamte produzierte elektrische Leistung ins Netz eingespeist. Die momentane Leistung $P(t)$ einer PV-Anlage ist abhängig von der momentanen kurzwelligen Strahlung, die auf die PV-Module einfällt, der Modulfläche $A$ sowie den Wirkungsgraden der PV-Module und Leistungselektronik.

$$P(t) = I_\textrm{glob,t}(t) \cdot \eta_\textrm{tot} \cdot A$$

Anstatt der Fläche $A$ ist die Nennleistung $P_\textrm{nom}$ ein gängiges Mass für die Leistungsfähigkeit einer PV-Anlage. Sie beschreibt die Leistung, die ein PV-Modul bei Einstrahlung von 1000 W/m<sup>2</sup> ( Strahlungsspektrum gemäss Norm ) und einer Modul-Temperatur von 25 °C abgibt.

$$P_\textrm{nom} = I_\textrm{Std} \cdot \eta_\textrm{DC,25°C} \cdot A$$

Damit kann die momentane PV-Leistung durch die Nennleistung anstatt der Modulfläche ausgedrückt werden:

$$P(t) = \frac{I_\textrm{glob,t}(t)}{I_\textrm{Std}} \cdot \frac{\eta_{\textrm{DC,}T}}{\eta_\textrm{DC,25°C}} \cdot \eta_\textrm{AC} \cdot P_\textrm{nom}$$

Unter realen Bedingungen erzeugen PV-Anlagen über ein Jahr gesehen weniger Energie, als mit der Formel oben berechnet würde. Die Gründe dafür sind vielfältig:

- Schattenwurf auf die PV-Module
- Schlechterer Ertrag bei sehr flachem Einfallswinkel der Strahlung
- Schneeabdeckung
- Ausfall von Modulen oder Wechselrichtern
- Verschmutzung und Degradation

Diese Einflussfaktoren werden einzeln nicht genau quantifiziert, sondern in einer «Performance Ratio» berücksichtigt. Um diesen Faktor wird der theoretische Stromertrag «verschmiert» über das ganze Jahr geschmälert. Somit ergibt sich die reale Ertragsleistung als

$$P_\textrm{real}(t) = \frac{I_\textrm{glob,t}(t)}{I_\textrm{Std}} \cdot f_\textrm{PR} \cdot P_\textrm{nom}$$

Die Kalibrierung ergibt ein «Performance Ratio» von $f_\textrm{PR} = 0.715$.


#### Einfallende kurzwellige Strahlung

Die gesamte einfallende kurzwellige Strahlung ( Globalstrahlung, Index «glob» ) auf ein geneigtes ( tilted, Index «t» ) PV-Modul setzt sich zusammen aus dem Anteil der direkten Sonnenstrahlung ( gerichtete Strahlung ) und der diffusen Strahlung ( ungerichtet ).

$$I_\textrm{glob,t} = I_\textrm{dir,t} + I_\textrm{dif,t}$$

An den Strahlungsmessstationen wird die einfallende kurzwellige Strahlung auf eine horizontale Fläche ( Index «h» ) gemessen, welche sich ebenfalls aus direkter und diffuser Strahlung zusammensetzt.

$$I_\textrm{glob,h} = I_\textrm{dir,h} + I_\textrm{dif,h}$$

Als Messdaten liegen jeweils die Globalstrahlung und die Diffusstrahlung vor. Die direkte Strahlung kann entsprechend daraus berechnet werden. Ist die Ausrichtung eines PV-Moduls bekannt, können die direkte Strahlung und die Diffusstrahlung auf das Modul aus der Einstrahlung auf eine horizontale Fläche berechnet werden.

$$I_\textrm{dif,t} = I_\textrm{dif,h} \cdot \frac{1 + \cos(\beta)}{2}$$

und

$$I_\textrm{dir,t} = I_\textrm{dir,h} \cdot \frac{\cos(\theta_\textrm{i})}{\cos(\theta_\textrm{z})}$$

mit

$$\cos(\theta_\textrm{i}) = \sin(\theta_\textrm{e}) \cdot \cos(\beta) + \cos(\theta_\textrm{e}) \cdot \sin(\beta) \cdot \cos(\psi - \theta_\textrm{a})$$

wobei die Bedeutung der einzelnen Winkel in der Abbildung dargestellt sind.


###### Abbildung 4.6: Berechnung des Einstrahlungswinkels $\theta_\textrm{i}$ der Sonne auf eine gegenüber der Horizontalen um $\beta$ geneigten PV-Fläche mit der azimutalen Ausrichtung $\psi$. $\theta_\textrm{a}$ ist der Azimutwinkel der Sonne und $\theta_\textrm{e}$ der Winkel zwischen der Sonne und dem Horizont ( «elevation» ).

![Hier steht die Bildbeschreibung](../img/angels.svg)

#### Kalibrierung

Die berechnete PV-Leistung wird mit realen Ertragsdaten aus ca. Schweizer 2000 PV-Anlagen kalibriert.

Aus der Energiestatistik müssen eine örtliche Verteilung sowie eine Ausrichtungsverteilung der existierenden PV-Anlagen angenommen werden. Für die örtliche Verteilung wird die Gewichtung nach Bevölkerung gemäss der Tabelle oben angenommen. 
