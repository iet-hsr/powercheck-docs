# 7. Auswertung und Interpretation

## Aufbau der Diagramme
Je nach gewünschter Bilanzierungszeit (monatlich, täglich, viertelstündlich), bestehen die Diagramme aus je einem waagrechten Balken oder unterscheidlich vielen senkrechten Säulen.
Darin sind die einzelnen Energieerzeuger (im Diagramm "Produktion"), Energieverbaucher (im Diagramm "Verbrauch") und Speichertypen (in den Diagrammen "Speicherinhalt" und "Speicher Zu- und Abflüsse") anhand ihrer Farbe sehr deutlich erkenn- und unterscheidbar. Im Produktions- resp. Verbauchsdiagramm ist der Gesamtverbrauch resp. die Gesamtproduktion als schwarze Linie dargestellt. Dadurch können Produktion und Verbrauch sehr rasch gegenübergestellt werden, wodurch das Erkennen von Defiziten oder Überschüssen leicht fällt. 

## Viertelstündliche, tägliche, monatliche, jährliche Bilanzierung
Bei allen allen Diagrammen kann je nach Wunsch der zeitlichen Auflösung die Dauer der Bilanzierung gewählt werden. 

### Jahresbilanzierung
#### Merkmale
Die grafische Jahresbilanzierung von Produktion, Verbrauch, Speicherstand, Zu- und Abflüsse ist nach dem Ausführen einer ersten Berechnung standardmässig aktiviert. Die Jahresbilanzierung ist sehr schnell am liegenden Balken zu erkennen.   
#### Hintergrund
Die Jahresbilanzierung bildet die Gesamtbilanz für Produktion, Verbrauch, Speicherstand und Zu- und Abflüsse eines simulierten Jahres in grafischer Form ab. Das heisst, dass alle Produktions-, Verbrauchs-, Speicher-, Zu- und Abflussaktivitäten eines Jahres von allem was sich im simulierten Stromnetz befindet, für ein ganzes Jahr addiert und als waagrechter Balken dargestellt wird.

![Jahresbilanzierung der Energieproduktion](../img/production_yearly.png)
###### Diagramm mit Jahresbilanz der Produktion aus dem berechneten Szenario "Daten aus 2019 (ohne Import/Export)"

### Monatsbilanzierung
#### Merkmale
Die grafische Monatsbilanzierung von Produktion, Verbrauch, Speicherstand und Zu- und Abflüsse ist an den zwölf senkrechten Säulen zu erkennen. Für jeden Monat eines Jahres ist eine eigene Säule abgebildet.     
#### Hintergrund
Die Monatsbilanzierung bildet die monatliche Bilanzierung für Produktion, Verbrauch, Speicherstand und Zu- und Abflüsse eines simulierten Jahres in grafischer Form ab. Das heisst, dass alle Produktions-, Verbrauchs-, Speicher-, Zu- und Abflussaktivitäten eines Jahres von allem was sich im simulierten Stromnetz befindet, für jeden Monat addiert und mittels zwölf stehenden Säulen dargestellt werden.

![Monatsbilanzierung der Energieproduktion](../img/production_monthly.png)
###### Diagramm mit Monatsbilanz der Produktion aus dem berechneten Szenario "Daten aus 2019 (ohne Import/Export)"

### Tagesbilanzierung
#### Merkmale
Die grafische Tagesbilanzierung von Produktion, Verbrauch, Speicherstand und Zu- und Abflüsse ist am nahezu vollständig gefüllten Diagramm, bestehend aus 365 stehenden Säulen, zu erkennen. Für jeden Tag des Jahres ist eine eigene Säule erzeugt. 
#### Hintergrund
Die Tagesbilanzierung bildet die tägliche Bilanzierung für Produktion, Verbrauch, Speicherstand und Zu- und Abflüsse eines simulierten Jahres in grafischer Form ab. Das heisst, dass alle Produktions-, Verbrauchs-, Speicher-, Zu- und Abflussaktivitäten eines Jahres von allem was sich im simulierten Stromnetz befindet, für jeden Tag addiert und dann mit 365 senkrechten Säulen dargestellt werden.

![Tagesbilanzierung der Energieproduktion](../img/production_daily.png)
###### Diagramm mit Tagesbilanz der Produktion aus dem berechneten Szenario "Daten aus 2019 (ohne Import/Export)"
Deutlicher wird die Tagesbilanzierung, wenn das Diagramm auf einen bestimmten Zeitbereich vergrössert wird. Im folgenden Beispiel auf den Zeitbereich vom 6. bis zum 18. August. Für jeden Tag in diesem Zeitfenster wird eine eigene Bilanz gebildet und als Säule dargestellt.

![Vergrösserte Diagrammansicht der Tagesbilanzierung der Energieproduktion](../img/production_daily2.png)
###### Vergrösserte (mittels Zoomfunktion) Diagrammansicht mit Tagesbilanz der Produktion aus dem berechneten Szenario "Daten aus 2019 (ohne Import/Export)"

### 15-Minuten-Bilanzierung
#### Merkmale 
Die grafische Bilanzierung in 15-Minuten-Auflösung von Produktion, Verbrauch, Speicherstand und Zu- und Abflüsse ist am vollständig gefüllten Diagramm, bestehend aus stetig verlaufenden Kurven, zu erkennen. Diese Bilanzierung orientiert sich an der internen Berechnungsauflösung des PowerChecks, welche ebenfalls mit 15-Minuten-Zeitschritten arbeitet. So wird quasi die direkte Berechnungsbilanz ohne zeitliche Kumulation dargestellt. 
#### Hintergrund
Die 15-Minuten-Bilanzierung bildet die Bilanzierung jedes Datenwerts der Datenbank mit hostorischen Werten für Produktion, Verbrauch, Speicherstand und Zu- und Abflüsse eines simulierten Jahres in grafischer Form ab. Das heisst, dass alle Produktions-, Verbrauchs-, Speicher- und Zu- und Abflussaktivitäten eines Jahres von allem was sich im simulierten Stromnetz befindet, direkt mit stetig verlaufenden Kurven dargestellt werden.

![15-Minuten-Bilanzierung der Energieproduktion](../img/production_quarterly.png)
###### Diagramm mit 15-Minuten-Bilanz der Produktion aus dem berechneten Szenario "Daten aus 2019 (ohne Import/Export)"
Auch in der folgenden Ansicht wird die 15-Minuten-Bilanzierung deutlich, wenn das Diagramm auf einen bestimmten Zeitbereich vergrössert wird. Im folgenden Beispiel auf den Zeitbereich vom 13. bis zum 22. August. 

![Vergrösserte Diagrammansicht der 15-Minuten-Bilanzierung der Energieproduktion](../img/production_quarterly2.png)
###### Vergrösserte (mittels Zoomfunktion) Diagrammansicht mit 15-Minuten-Bilanz der Produktion aus dem berechneten Szenario "Daten aus 2019 (ohne Import/Export)"

## Speicher-Informationen
Die optionalen Speicher-Informationen werden neben dem Produktions- und Verbrauchsdiagramm zusätzlich in grafischer Form abgebildet. Standardmässig werden diese Informationen ausgeblendet. Durch Klick auf den Button "Einblenden" wird der entsprechende Reiter bestehend aus den Diagrammen "Speicherinhalt" und "Speicher Zu- und Abflüsse" geöffnet.

### Speicherinhalt
In diesem Diagramm ist je nach gewünschter Bilanzierungsform (monatlich, täglich, viertelstündlich) in unterscheidlich vielen stehenden Säulen dargestellt, wieviel Energie zum jeweiligen Zeitpunkt in allen verfügbaren Energiespeichern (Stauseen, Pumpspeicher, Batteriespeicher) gespeichert ist. Eine grafische Jahresbilanzierung ist aufgrund ihrer begrenzten Aussagekraft nicht einstellbar.

![Monatsbilanzierung des Speicherinhalts](../img/storage.png)
###### Diagramm mit Monatsbilanzierung des Speicherinhalts aus dem berechneten Szenario "Daten aus 2019 (ohne Import/Export)"
#### Nachhaltige Speicher-Nutzung
In einem sinnvollen Energieszenario muss die Nutzung aller Schweizer Energie-Speicher über den Zeitraum eines Jahres nachhaltig sein. Konkret lässt sich die Thematik wie folgt erklären:
- __Am Jahresende weniger Energieinhalt als zum Jahresbeginn:__ Das Energieszenario ist NICHT nachhaltig. Über das gesamte Jahr wird mehr Energie verbraucht als produziert wird. Somit werden die Energiespeicher längerfristig entleert. Energieversorgungslücken wären die Folge, welche nur durch Stromimporte gedeckt werden könnten.
- __Am Jahresende gleich viel Energieinhalt wie zum Jahresbeginn:__ Das Energieszenario ist nachhaltig und ausbalanciert. Energie, welche zu bestimmten Zeitpunkten eines Produktionsdefizits im Verlauf des Jahres aus den Speichern bezogen worden ist, wird zu Zeiten eines Produktionsüberschusses wieder gespeichert. 
- __Am Jahresende mehr Energieinhalt als zum Jahresbeginn:__ Über das Jahr hindurch wird mehr elektrische Energie erzeugt als verbraucht wird. Dies weil die Erzeugerseite zu gross ausgelegt ist. Überschüssige Energie wird gespeichert. Längerfritig werden die Speicher an ihre Kapazitätsgrenze kommen, wodurch dann weiterer überschüssiger Strom nur noch exportiert werden kann. 


### Speicher Zu- und Abflüsse
#### Speichertypen
In diesem Diagramm werden Zu- und Abflüsse der Speicher dargestellt, welche für die einzelnen Speichertypen wie folgt zu interpretieren sind:
- __Batteriespeicher:__ Zu- und Abflüsse sind bei diesem Speichertyp elektrische Energiemengen, welche in die Batteriespeicher geladen resp. entladen worden sind. Im Diagramm wird diese Energie effizienzbereinigt in Gigawattstunden (GWh) dargestellt.
- __Pumpspeicher:__ Zu- und Abflüsse sind bei diesem Speichertyp Wassermengen, welche einerseits den Speicherseen entnommen und mittels Turbinierung verstromt werden und andererseits mit Strom über Pumpen in die Speicherseen hochgepumpt werden. Im Diagramm werden diese Wassermengen entsprechend ihrem Energie-Äquivalent effizienzbereinigt in Gigawattstunden (GWh) dargestellt.  
- __Stauseen:__ Abflüsse sind bei diesem Speichertyp Wassermengen, welche den Stauseen entnommen und mittels Turbinierung verstromt werden. Da Stauseekraftwerke über keine Pumpmöglichkeit verfügen, sind Zuflüsse hier lediglich natürliche Niederschläge und Schneeschmelzen. Im Diagramm werden diese Wassermengen entsprechend ihrem Energie-Äquivalent effizienzbereinigt in Gigawattstunden (GWh) dargestellt.
#### Darstellung
Je nach gewünschter Bilanzierungszeit (monatlich, täglich, viertelstündlich) werden die Zu- und Abflüsse als waagrechter Balken oder unterscheidlich vielen senkrechten Säulen dargestellt. Die Nullbilanz ist dabei mittig positioniert. Da Zuflüsse den Speichersystemen zugeführte Energiemengen sind, werden diese entsprechend mit positivem Vorzeichen im Diagramm dargestellt. Abflüsse sind wiederum von den Speichersystemen abgeführte Energiemengen, welche wiederum mit negativem Vorzeichen dargestellt werden. 

![Jahresbilanzierung des der Zu- und Abflüsse](../img/fluxes.png)
###### Diagramm der Zu- und Abflüsse aller Speicher als Jahresbilanzierung aus dem berechneten Szenario "Daten aus 2019 (ohne Import/Export)"


## Import & Export 
Neben den Erzeugern und dem Endverbauch können zusätzlich historische Import- und Exportdaten ausgewählt werden, welche direkt vom Schweizer Stromnetzbetreiber Swissgrid stammen. Dadurch ist es möglich, das Schweizer Stromnetz komplett abzubilden.
In den hier abgebildeten Produktions- und Verbrauchsdiagrammen sind Import und Export des Jahres 2019 in 15-Minuten-Auflösung abgebildet. Was auffällt ist, dass zeitgleich Import als auch Export stattfindet. Dies geschieht z.B. aufgrund von Stromhandel, welcher vor allem mithilfe der Pumpspeicherkraftwerke betrieben wird.

![Produktions- und Verbrauchsdiagramme mit 15-Minuten-Bilanzierung mit isolierter Darstellung von Import resp. Export](../img/import_export.png)
###### Produktions- und Verbrauchsdiagramme mit 15-Minuten-Bilanzierung mit isolierter Darstellung von Import resp. Export aus dem berechneten Szenario "Daten aus 2019 (ohne Import/Export)" 

### Eigene Import- und Exportprofile
Über den Reiter "Benutzerdefiniert" können eigene Import- und Exportprofile erstellt, hochgeladen und mitsimuliert werden!

![Hochgeladenes Exportprofil als Zusatzverbrauch](../img/add_user_export.png)
###### Hochgeladenes Exportprofil (Beispieldaten aus 2019) als Zusatzverbrauch in "Benutzerdefiniert" zur Anfertigung eines eigenen Szenarios.


