# 6. Stromspeicher

In diesem Kapitel finden Sie Hintergrundinformationen zu den verschiedenen Typen von Stromspeichern.

## Stauseen

Der initiale Füllstand betrug in den vergangenen zehn Jahren am 1. Januar jeweils 45 bis 65 % der Gesamtkapazität.
   
###### Tabelle 6.1: Energieerzeugung aus Schweizer Speicherseen in den letzten Jahren. Quellen: Leistungsangaben: Bundesamt für Energie (BFE) - Statistik der Wasserkraftanlagen der Schweiz 2014/2015/2016/2017/2018/2019 (Excel); Energieangaben: Bundesamt für Energie (BFE) - Schweizerische Elektrizitätsstatistik 2019 (S. 13); Füllstandsangaben: Bundesamt für Energie (BFE) - Füllungsgrad der Speicherseen Wochenberichte 2013, 2014, 2015, 2016, 2017, 2018, 2019

| Jahr     | Installierte Leistung (GW) | Jahresenergieertrag* (GWh) | Füllstand am 1.1.** (GWh)  | Gesamtkapazität (nutzbar, GWh) |
| :------- | -------------------------: |--------------------------: |--------------------------:  |---------------------: |
| 2014 |                      8.744 |                     22'065 |                      5'261 |                 8'798 |
| 2015 |                      8.584 |                     22'891 |                      5'482 |                 8'799 |
| 2016 |                      8.915 |                     19'752 |                      4'073 |                 8'816 |
| 2017 |                      8.746 |                     20'720 |                      4'496 |                 8'833 |
| 2018 |                      8.752 |                     20'520 |                      4'361 |                 8'846 |
| 2019 |                      8.752 |                     22'856 |                      5'741 |                 8'846 |

\* Die Schweizerische Elektrizitätsstatistik unterscheidet nicht explizit zwischen Stauseekraftwerken und Pumpspeicherkraftwerken. Deshalb handelt es sich bei den Jahresenergieerträgen um kombinierte Werte.\
\** Beim initialen Füllstand handelt es sich um den Füllstand aller schweizweiten Speicherseen am 1. Januar des jeweiligen Jahres. Je nach Datum in den BFE-Wochenberichten sind die jeweils letzten und ersten Werte aus Wochenberichten zweier aufeinanderfolgender Jahre interpoliert worden.

### Zukünftige Entwicklung
Die Wasserkraft ist heute das wichtigste Standbein der erneuerbaren elektrischen Energieversorgung in der Schweiz. Der Ausbaustand hat ein Niveau erreicht, welcher sich in Zukunft nicht mehr entscheidend vergrössern lässt. Im Falle der Stauseen ist eine Erschliessung weiterer Gewässer nur noch bedingt möglich. Einzig eine Erhöhung von Dammmauern würde Potenzial bieten. [...weiterlesen](../de/future.md)

## Pumpspeicherkraftwerke

![Pumpspeicherkraftwerke](../img/public_Pump.jpg)

Entscheidend für die Speicherkapazität eines Pumpspeicherkraftwerks ist nicht nur das Oberbecken, sondern auch das Unterbecken. Nur wenn im Unterbecken genügend Wasser zum Hochpumpen zur Verfügung steht, kann im Oberbecken  ausreichend Wasser gespeichert werden.

###### Tabelle 6.2: Leistungen von Schweizer Pumpspeicherkraftwerke in den letzten Jahren. Quellen: Bundesamt für Energie (BFE) - Statistik der Wasserkraftanlagen der Schweiz 2014/2015/2016/2017/2018/2019 (Excel)

| Jahr     | Installierte Turbinenleistung (GW) |   Installierte Pumpenleistung (GW) |
| :------- | ---------------------------------: | ---------------------------------: |
| **2014** |                              3.898 |                              3.375 | 
| **2015** |                              3.898 |                              3.375 |
| **2016** |                              3.637 |                              3.102 |
| **2017** |                              4.137 |                              3.596 |
| **2018** |                              4.255 |                              3.647 |
| **2019** |                              4.255 |                              3.647 |

### Zukünftige Entwicklung
Aufgrund der Artverwandschaft stimmt die derzeitige Situation und das Entwicklungspotenzial der Pumpspeicherkraftwerke mit derer der Speicherseen überein. [...weiterlesen](../de/future.md)


## Batteriespeicher

![Pumpspeicherkraftwerke](../img/public_Battery.jpg)

Die Batteriespeicherkapazität des Schweizer Stromnetzes ist heute vernachlässigbar klein. Nichtsdestotrotz können Batteriespeicher für Zukunftsszenarien berücksichtigt werden.

#### EKZ Batteriespeicher

Der grösste Batteriespeicher der Schweiz hat die EKZ in Betrieb genommen. Er hat folgende Spezifikationen:

###### Tabelle 6.1: Spezifikationen des EKZ-Batteriespeichers. Quelle: ekz.ch/batterie.
|                                        |                       |
| :------------------------------------- | :-------------------- |
| **Maximale Ladeleistung**              | 18 MW                 |
| **Maximale Entladeleistung**           | 18 MW                 |
| **Speicherkapazität**                  | 7.5 MWh               |
| **Batterietyp**                        | Lithium-Ionen         |
| **Gesamtanzahl an Batteriemodulen**    | 1428                  |
| **Gewicht pro Container**              | 50 t                  |
| **Anzahl Container**                   | 3                     |
| **Batterielebensdauer**                | 10 Jahre (garantiert) |
| **Hersteller**                         | NEC, LG Chem          |
| **Gesamtkosten**                       | 6 Mio. CHF         |

