# Glossar

#### BFE
Bundesamt für Energie

#### Endverbrauch
Der Endverbrauch bezeichnet die Leistung respektive Energiemenge, die tatsächlich beim Endverbraucher (Haushalte, Industrie, ...) ankommt.

#### Flusskraftwerk
In der Terminologie des BFE werden Flusskraftwerk als Laufwasserkraftwerke bezeichnet. Flusskraftwerk scheint uns der allgemein verständlichere Begriff zu sein.

#### Import/Export
Es können vorgegebene Leistungsprofile über 1 Jahr für Stromimport und -export verwendet werden. Das dient in erster Linie dazu, den Ist-Zustand der vergangenen Jahre abzubilden, denn die historischen Import- und Exportprofile sind bekannt und öffentlich verfügbar. 

#### Mangel (Strommangel)
Ein Strommangel wird detektiert, wenn die gesamte Produktionsleistung die Stromnachfrage nicht zu decken vermag. Ein Strommangel kann in Realität entweder durch Stromimport ausgeglichen werden, oder indem zusätzliche inländische Produktionskapazitäten wie beispielsweise Notstromaggregate aktiviert werden.

#### Pumpspeicherwerke resp. Pumpspeicher
Diese Begriffe werden für Einrichtungen verwendet, die Wasser von einem Reservoir in ein höhergelegenes Reservoir pumpen und somit Energie speichern können. Durch turbinieren wird daraus wieder Strom erzeugt. Die Pumpspeicherwerke in PowerCheck haben keine natürlichen Zuflüsse. In der Terminologie des BFE werden diese Werke "Umwälzwerke" genannt. Aufgrund der besseren Verständlichkeit bezeichen wir sie als Pumpspeicherwerke. Dieser Begriff wird vom BFE wiederum verwendet für Speicherseen, die sowohl natürliche wie künstliche Zuflüsse (durch Pumpen) haben.

#### Selbstversorgungsgrad
Der Selbstversorgungsgrad gibt an, welchen Anteil der nachgefragten Energie (inkl. Speicherpumpen und Export) übers Jahr gesehen gleichzeitig im Inland produziert wird. Ein Selbstversorgungsgrad von 100 % bedeutet also, dass die Stromnachfrage jederzeit durch inländische Produktion gedeckt werden kann.

Es kann der Fall auftreten, bei welchem die Stromnachfrage jederzeit durch inländische Produktion gedeckt wird, der gesamthafte Speicherstand (Stauseen, Pumpspeicher, Batteriespeicher) jedoch Ende Jahr tiefer ist als Anfang Jahr. In diesem Fall ist das Szenario nicht nachhaltig. Dieser Fall wird bezüglich Berechnung des Selbstversorgungsgrads so behandelt, als ob Ende Jahr soviel Strom importiert wird, so dass die Speicher Ende Jahr den gleichen Füllstand aufweisen wie Anfang Jahr. Der Selbstversorgungsgrad liegt dann also unter 100 %.

Der Selbstversorgungsgrad ist in erster Linie eine politische/ökonomische und nicht eine technische Kennzahl. Das Schweizer Stromnetz hängt mit dem europäischen Netz zusammen, und diese stabilisieren sich gegenseitig. Die Frage ist einfach, wie stark will die Schweiz von Stromimporten abhängig sein. Der Selbstversorgungsgrad zeigt diese Auslandsabhängigkeit auf.

#### Solarenergie
Solarenergie wird bei PowerCheck als Synonym zu Photovoltaik verwendet. Solarenergie entspricht eher der Umgangssprache als Photovoltaik.