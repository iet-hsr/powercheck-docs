module.exports = {
  dest: 'build/docs/',
  base: '/docs/',
  locales: {
    // The key is the path for the locale to be nested under.
    // As a special case, the default locale can use '/' as its path.
    // TODO: Uncomment this when en pages are ready
    // '/en/': {
    //   lang: 'en-US', // this will be set as the lang attribute on <html>
    //   title: 'PowerCheck Documentation',
    //   description: 'Documentation for the PowerCheck app',
    // },
    '/de/': {
      lang: 'de-CH',
      title: 'PowerCheck Dokumentation',
      description: 'Dokumentation zur PowerCheck-App',
    },
  },

  /**
   * Extra tags to be injected to the page HTML `<head>`
   *
   * ref：https://v1.vuepress.vuejs.org/config/#head
   */
  head: [
    ['meta', {
      name: 'theme-color',
      content: 'rgb(222,75,64)',
    }],
    ['meta', {
      name: 'apple-mobile-web-app-capable',
      content: 'yes',
    }],
    ['meta', {
      name: 'apple-mobile-web-app-status-bar-style',
      content: 'black',
    }],
    process.env.CONTEXT === 'production'
      ? ['script', {
        async: true,
        defer: true,
        'data-domain': 'powercheck.ch',
        src: 'https://plausible.powercheck.ch/js/index.js',
      }]
      : []
  ],

  /**
   * Theme configuration, here is the default theme configuration for VuePress.
   *
   * ref：https://v1.vuepress.vuejs.org/theme/default-theme-config.html
   */
  themeConfig: {
    // sidebar: [
    //   { title: "Kurzanleitung", children: [""] },
    //   {
    //     title: "Misc",
    //       children: ["more"],
    //   },
    // ],
    locales: {
      '/en/': {
        label: 'English',
        selectText: 'English',
        sidebar: [
          '/en/quick-start',
          '/en/user-manual',
          '/en/intro',
          '/en/calculation-methodology',
          '/en/producers-consumers',
          '/en/storage',
          '/en/conclusions',
          '/en/future',
          '/en/sources',
          '/en/glossary',
        ],
      },
      '/de/': {
        label: 'Deutsch',
        selectText: 'Deutsch',
        sidebar: [
          '/de/quick-start',
          '/de/user-manual',
          '/de/intro',
          '/de/calculation-methodology',
          '/de/producers-consumers',
          '/de/storage',
          '/de/conclusions',
          '/de/future',
          '/de/sources',
          '/de/glossary',
        ],
      },
    },
  },

  /**
   * Apply plugins，ref：https://v1.vuepress.vuejs.org/zh/plugin/
   */
  plugins: [
    '@vuepress/plugin-back-to-top',
    '@vuepress/plugin-medium-zoom',
    [
      'vuepress-plugin-mathjax',
      {
        target: 'chtml',
        macros: {
          '*': '\\times',
        },
      },
    ],
  ],
};
