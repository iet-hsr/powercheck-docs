module.exports = {
  // this replicates having every vue file from the default theme in the ./vuepress/themes directory
  // without actually having to have them physically there
  extend: '@vuepress/theme-default',
};